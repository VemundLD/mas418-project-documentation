ROS
===
The ROS part of the program consists of a single node, ads_node, that handles both ADS communication with TwinCat and visualization in Rviz.
The ADS node is part of the crane_controller ros package.

ADS communication
-----------------

To get ADS working, clone the official ADS repository from Beckhoff into the workspace

.. code-block:: bash

    git clone https://github.com/Beckhoff/ADS.git

From inside the new ADS directory, run the following commands to build the ADS library

.. code-block:: bash

    meson setup build
    ninja -C build




The ADS communication is based on an example ROS node provided in the course.
First, the ADS variables are initialized. These must match with the corresponding variables in the TwinCAT program exactly.

.. code-block:: c++

    namespace craneads {

    struct AdsVariables
    {
        AdsVariables() = delete;

        explicit AdsVariables(AdsDevice& route)
            : activateMotion{route, "MAIN.bActivateMotion"}
            , velocityReference{route, "MAIN.fVelRef"}
            , positionReference{route, "MAIN.fPosRef"}
            , positionMeasurement{route, "MAIN.fPosMeas"}
            , PistonPressure{route, "MAIN.PistonPressure"}
            , RodSidePressure{route, "MAIN.RodSidePressure"}
        {
            
        }

        AdsVariable<bool> activateMotion;
        AdsVariable<double> velocityReference;
        AdsVariable<double> positionReference;
        AdsVariable<double> positionMeasurement;
        AdsVariable<double> PistonPressure;
        AdsVariable<double> RodSidePressure;
        
    };

The AdsHandler class handles the sending of the data to the PLC. It contains methods to send and receive data over TCP/IP.

.. code-block:: c++

    class AdsHandler
    {
    public:
        explicit AdsHandler(const AmsNetId remoteNetId, const std::string remoteIpV4)
            : remoteNetId_(remoteNetId)
            , remoteIpV4_(remoteIpV4)
            , route_{remoteIpV4_, remoteNetId_, AMSPORT_R0_PLC_TC3}
            , ads_(route_) { }

        AdsHandler() : AdsHandler({127, 0, 0, 1,  1, 1}, "127.0.0.1") { }


        void activateMotion()
        {
            ads_.activateMotion = true;
        }

        void deactivateMotion()
        {
            ads_.activateMotion = false;
        }

        void setVelocityReference(double value)
        {
            ads_.velocityReference = value;
        }

        void setPositionReference(double value)
        {
            ads_.positionReference = value;
        }

        double getPositionMeasurement()
        {
            return ads_.positionMeasurement;
        }

        double getPistonPressure()
        {
            return ads_.PistonPressure;
        }

        double getRodSidePressure()
        {
            return ads_.RodSidePressure;
        }


        void printState()
        {
            const auto state = route_.GetState();
            std::cout << "ADS state: "
                      << std::dec << static_cast<uint16_t>(state.ads)
                      << " devState: "
                      << std::dec << static_cast<uint16_t>(state.device);
        }

        ~AdsHandler() { }

    private:
        const AmsNetId remoteNetId_;
        const std::string remoteIpV4_;
        AdsDevice route_;
        AdsVariables ads_;
    };

}


Sending and receiving data in publisher class
---------------------------------------------
The node also contains a simple publisher node. This node both sends position- and velocity references to TwinCAT and reads data from the PLC.

The node has two publishers. One for publishing a string message on the pressure topic with the cylinder and rod side pressures.

The other publisher publishes a joint state message on the joint_states topic that tells Rviz where the joint between the crane frame and boom is currently located.

Both publishers have an associated timer that calls a timer callback function at the desired rate, in this case every 10ms.

.. code-block:: c++

    class MinimalPublisher : public rclcpp::Node
    {
    public:
        MinimalPublisher() : Node("minimal_publisher"), count_(0)
        {
        test_publisher_ = this->create_publisher<std_msgs::msg::String>("test_topic", 10);
        test_timer_ = this->create_wall_timer(10ms, std::bind(&MinimalPublisher::pressure_timer_callback, this));
        
        position_publisher_ = this->create_publisher<sensor_msgs::msg::JointState>("joint_states", 10);
        position_timer_ = this->create_wall_timer(10ms, std::bind(&MinimalPublisher::position_timer_callback, this));
        }

The pressure callback creates a String message and fills it with the pressures retrieved from the ads handler via getPistonPressure() and getRodSidePressure().
It then prints the message to the terminal befor publishing the message on the relevant topic.

.. code-block:: c++

    private:
    void pressure_timer_callback()
    {  
      const AmsNetId remoteNetId { 192, 168, 0, 10, 1, 1 };
      const std::string remoteIpV4 = "192.168.0.10";
      craneads::AdsHandler adsHandler(remoteNetId, remoteIpV4);
  
      auto message = std_msgs::msg::String();
      message.data = "Piston Pressure: " + std::to_string(adsHandler.getPistonPressure()) + "\t" + "Rod Side Pressure " + std::to_string(adsHandler.getRodSidePressure());
      RCLCPP_INFO(this->get_logger(), "Pressures from Simulator: '%s'", message.data.c_str());
      test_publisher_->publish(message);
    }


The position callback reads the boom angle calculated in the crane simulation on the PLC, and converts it to radians.
Then a joint state message is created and filled with a timestamp, the name of the joint to be moved and the position of the joint.
Once the message is filled, it is published on the joint_states topic.

After the joint state message is published, the callback function sets the position and velocity reference for the auto mode. 


.. code-block:: c++

    void position_timer_callback()
    {  
      const AmsNetId remoteNetId { 192, 168, 0, 10, 1, 1 };
      const std::string remoteIpV4 = "192.168.0.10";
      craneads::AdsHandler adsHandler(remoteNetId, remoteIpV4);
      
      
      double boomAngle = -adsHandler.getPositionMeasurement() * PI / 180;
      
      auto message = sensor_msgs::msg::JointState();  
      message.header.stamp = this->get_clock()->now();
      message.name.push_back("base_to_crane_boom");
      message.position.push_back(boomAngle);  
      position_publisher_->publish(message);
      
      adsHandler.setPositionReference(static_cast<double>(generateWave()));
      adsHandler.setVelocityReference(static_cast<double>(1.0));
    }

Lastly, the timers and publishers are declared. 

.. code-block:: c++

    rclcpp::TimerBase::SharedPtr test_timer_;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr test_publisher_;
    
    rclcpp::TimerBase::SharedPtr position_timer_;
    rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr position_publisher_;
    size_t count_;


in the main function, the ads handler is set up and the publisher node is started

.. code-block:: c++

    int main(int argc, char * argv[])
    {
    std::cout << "ROS2 ADS node starting up.." << std::endl;

    // Real lab PLC IP.
    const AmsNetId remoteNetId { 192, 168, 0, 10, 1, 1 };
    const std::string remoteIpV4 = "192.168.0.10";


    std::cout << "  Create AdsHandler.. ";
    craneads::AdsHandler adsHandler(remoteNetId, remoteIpV4);
    std::cout << "  OK" << std::endl;

    adsHandler.deactivateMotion();

    adsHandler.printState();

    adsHandler.setVelocityReference(0);
    std::this_thread::sleep_for (std::chrono::seconds(5));
    adsHandler.setPositionReference(3.14);

    adsHandler.activateMotion();
    std::ofstream posfile;
    
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<MinimalPublisher>());
    rclcpp::shutdown();

    return 0;
    }


visualization in Rviz
---------------------
In order to visualize the crane in Rviz, a description of the crane is needed. This is described in a Universal Robot Description Format (URDF) file. 

The URDF is written in xml format and contains information about the crane's parts and the connections between them. 

.. code-block:: xml

    <?xml version="1.0"?>
    <robot xmlns:xacro="" name="robot">
    
    <material name="green">
    	<color rgba="0 1 0 1" />
    </material>


    <link name="base_link">
        <visual>
            <origin rpy="0 0 0" xyz="0 0 0.57207"/>
            <geometry>
                <mesh filename="package://crane_controller/meshes/frame.stl"/>
            </geometry> 
            <material name="green"/>
        </visual>
    </link>
    
    
    <joint name="base_to_crane_boom" type="continuous">
        <parent link="base_link" />
        <child link="crane_boom" />
        <origin rpy="0 0 1.57" xyz="-0.56876 0 1.752"/>
    </joint>
    
    
   <link name="crane_boom">
        <visual>
            <origin rpy="0 0 -1.57" xyz="0 -3.108 0.063"/>
            <geometry>
                <mesh filename="package://crane_controller/meshes/boom.stl"/>
            </geometry> 
            <material name="green"/>
        </visual>
    </link>
    
    
    <joint name="boom_to_tooltip" type="fixed">
        <parent link="crane_boom" />
        <child link="tooltip" />
        <origin rpy="3.14159265 0 0" xyz="0 -3.6 0"/>
    </joint>
    
    
        
   <link name="tooltip">
    </link>



Running the program
-------------------

In three new terminals, remember to source the workspace from the directory where the package was built in all of them.

.. code-block:: bash

    source install/setup.bash


In the first terminal, run the ADS node

.. code-block:: bash

    ros2 run crane_controller ads_node

Now that the crane is described, Rviz needs to load in the machine description. For Rviz to find it, the 
robot description must be published on the /robot_description topic. This is done by running robot_state_publisher
with the path to the URDF file.

.. code-block:: bash

    ros2 run robot_state_publisher robot_state_publisher <path to URDF>


Finally in the third terminal, run Rviz.

.. code-block:: bash

    rviz2

